package c.bottomnavigationarch.model

data class Question(
    val text: String,
    val answers: List<String>
)