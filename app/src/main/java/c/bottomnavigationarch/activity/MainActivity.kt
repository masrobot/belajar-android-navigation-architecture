package c.bottomnavigationarch.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import c.bottomnavigationarch.R
import c.bottomnavigationarch.utils.gone
import c.bottomnavigationarch.utils.visible
import kotlinx.android.synthetic.main.activity_main.*

// Original concept from https://github.com/udacity/andfun-kotlin-android-trivia/
// This version using Bottom Navigation, Udacity version using Navigation Drawer

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.titleFragment, R.id.rulesFragment, R.id.aboutFragment))

        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.titleFragment -> nav_view.visible()
                R.id.rulesFragment -> nav_view.visible()
                R.id.aboutFragment -> nav_view.visible()
                else -> nav_view.gone()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
