package c.bottomnavigationarch.fragment


import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import c.bottomnavigationarch.R
import kotlinx.android.synthetic.main.fragment_game_won.*
import org.jetbrains.anko.sdk15.coroutines.onClick

// Original concept from https://github.com/udacity/andfun-kotlin-android-trivia/
// This version using Bottom Navigation, Udacity version using Navigation Drawer

class GameWonFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_won, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        btn_next_match.onClick {
            findNavController().navigate(GameWonFragmentDirections.actionGameWonFragmentToTitleFragment())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.winner_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.share -> {
                val args = GameWonFragmentArgs.fromBundle(arguments as Bundle)
                val shareIntent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(
                        Intent.EXTRA_TEXT,
                        getString(R.string.share_success_text, args.numCorrect, args.numQuestions)
                    )
                    type = "text/plain"
                }
                startActivity(shareIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
