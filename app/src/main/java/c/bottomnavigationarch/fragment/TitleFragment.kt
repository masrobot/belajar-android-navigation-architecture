package c.bottomnavigationarch.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import c.bottomnavigationarch.R
import kotlinx.android.synthetic.main.fragment_title.*
import org.jetbrains.anko.sdk15.coroutines.onClick

// Original concept from https://github.com/udacity/andfun-kotlin-android-trivia/
// This version using Bottom Navigation, Udacity version using Navigation Drawer

class TitleFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_title, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_play.onClick {
            findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToGameFragment())
        }
    }
}
